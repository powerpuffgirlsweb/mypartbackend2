<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class User_bl {

    public function login($userData) {

        //return $userData["username"];



        $user = User::getBy("username", $userData["username"]);

        if ($user != null) {

            if ($user->getpassword() == $userData["password"]) {
                $response[] = array("token" => True, "id" => $user->getId());
            } else {
                $response[] = array("token" => False, "id" => null);
            }
        } else {

            $response[] = array("token" => "error", "id" => null);
        }

        return $response;
    }

    public function registro($userData) {

        if (!is_null($userData)) {

            $user = User::getBy("username", $userData["username"]);
            $userEmail = User::getBy("email", $userData["email"]);

            if (is_null($user)) {
                if (is_null($userEmail)) {


                    $user2 = User::instanciate($userData);
                    $r = $user2->create();
                    $response[] = array("token" => "registro", "id" => $user2->getId());
                } else {

                    $response[] = array("token" => "emailExiste", "id" => null);
                }
            }

            if (!is_null($user)) {
                if (is_null($userEmail)) {


                    $response[] = array("token" => "usuarioExiste", "id" => null);
                    // $r = $user2->create();
                } else {

                    $response[] = array("token" => "usuarioEmailExiste", "id" => null);
                }
            }
        } else {
            $response[] = array("token" => "null", "id" => null);
        }
        return $response;
    }

    public function update($userData) {


        if (!is_null($userData)) {
            $UserLast = User::getById($userData["id"]);
            if (!is_null($UserLast)) {
                $UserNameExist = User::getBy("username", $userData["username"]);
                $EmailExiste = User::getBy("email", $userData["email"]);
                $UDisponible;
                $EDisponible;
                if ($UserLast->getUsername() != $userData["username"]) {
                    if (is_null($UserNameExist)) {
                        $UDisponible = True;
                    } else {
                        $UDisponible = False;
                        $Response[0] = array(
                            "estado" => False,
                            "token" => "El usuario ya existe");
                    }
                } else {
                    $UDisponible = True;
                }
                if ($UserLast->getEmail() != $userData["email"]) {
                    if (is_null($EmailExiste)) {
                        $EDisponible = True;
                    } else {
                        $EDisponible = False;
                        $Response[1] = array(
                            "token" => "El email ya existe");
                    }
                } else {
                    $EDisponible = True;
                }
                if ($UDisponible && $EDisponible) {
                    $User = User::instanciate($userData);
                    $User->setId($UserLast->getId());
                    $User->setUsername($userData["username"]);
                    $User->setName($userData["name"]);
                    $User->setLname($userData["lname"]);
                    $User->setEmail($userData["email"]);
                    $User->setPassword($userData["password"]);
                    $User->setPicture($userData["picture"]);
                    $User->update();
                    $Response[0] = array(
                        "token" => "El usuario se actualizo correctamente");
                }
            } else {
                $Response[0] = array(
                    "token" => "El usuario no existe");
            }
        } else {
            $Response[0] = array(
                "token" => "Introduzca algun dato");
        }
        return $Response;
    }

    public function solicitud($userData) {


        $i = 0;
        $i2 = 0;

        if (!is_null($userData)) {


            $team = Team::where("owner", $userData["id"]);

            if (count($team) > 0) {


                foreach ($team as $key => $value) {
                    $user = User::getById($userData["id"]);
                    $team[$i]["owner"] = $arr[] = array(
                        "id" => $user->getId(),
                        "nombre" => $user->getName()
                    );
                    $i++;
                }
                $Response[0] = array("token" => "myTeams", "team" => $team);
            } else {
                $Response[0] = array(
                    "token" => "nOwner");
            }

            $activity = Activity::where("responsable", $userData["id"]);


            if (count($activity) > 0) {


                foreach ($activity as $key => $value2) {
                    $user = User::getById($userData["id"]);
                    $activity[$i2]["responsable"] = $arr2[] = array(
                        "id" => $user->getId(),
                        "nombre" => $user->getName()
                    );
                    $i2++;
                }

                $Response[1] = array("token" => "myActivities", "actividades" => $activity);
            } else {
                $Response[1] = array(
                    "token" => "noActividades");
            }
        }
        return $Response;
    }
    
   

}
