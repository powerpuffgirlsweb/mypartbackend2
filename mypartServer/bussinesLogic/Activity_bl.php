<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Activity_bl {

    public function myActivities($userData) {

        $myActivities = Activity::where("responsable", $userData["id"]);

        return $myActivities;
    }

    public function reasignarActividades($arrayActivity) {

        if (!is_null($arrayActivity)) {

            foreach ($arrayActivity as $key => $value) {

                $activity = Activity::instanciate($value);
                $activity->setResponsable($value["responsable"]);
                $activity->update();
            }
        }
    }

}
