<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class User extends BModel {
    

  private $id, $username, $name, $lname, $email, $password, $picture;



function __construct($id, $username, $name, $lname, $email, $password, $picture) {
    $this->id = $id;
    $this->username = $username;
    $this->name = $name;
    $this->lname = $lname;
    $this->email = $email;
    $this->password = $password;
    $this->picture = $picture;
}

    
  private $has_many = array(
      'Team'=>array(
          'class'=>'Team',
          'my_key'=>'id',
          'other_key'=>'id',
          'join_as'=>'user',
          'join_with'=>'team',
          'join_table'=>'teamMembers',
         
          )
      );
  
  
  
      function getHas_many() {
          return $this->has_many;
      }

      function setHas_many($has_many) {
          $this->has_many = $has_many;
      }

      
public function getId() {
    return $this->id;
}

public function getUsername() {
    return $this->username;
}

public function getName() {
    return $this->name;
}

public function getLname() {
    return $this->lname;
}

public function getEmail() {
    return $this->email;
}

public function getPassword() {
    return $this->password;
}

public function getPicture() {
    return $this->picture;
}

public function setId($id) {
    $this->id = $id;
}

public function setUsername($username) {
    $this->username = $username;
}

public function setName($name) {
    $this->name = $name;
}

public function setLname($lname) {
    $this->lname = $lname;
}

public function setEmail($email) {
    $this->email = $email;
}

public function setPassword($password) {
    $this->password = $password;
}

public function setPicture($picture) {
    $this->picture = $picture;
}

public function getMyVars(){
        return get_object_vars($this);
    }

}