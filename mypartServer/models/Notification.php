<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Notification extends BModel {

    private $id, $subject, $to, $link, $isread;

    function __construct($id, $subject, $to, $link, $isread) {
        $this->id = $id;
        $this->subject = $subject;
        $this->to = $to;
        $this->link = $link;
        $this->isread = $isread;
    }

    private $has_one = array(
        'User' => array(
            'class' => 'User',
            'join_as' => 'to',
            'join_with' => 'id'
        )
    );

    function getHas_one() {
        return $this->has_one;
    }

    function setHas_one($has_one) {
        $this->has_one = $has_one;
    }

    function getId() {
        return $this->id;
    }

    function getSubject() {
        return $this->subject;
    }

    function getTo() {
        return $this->to;
    }

    function getLink() {
        return $this->link;
    }

    function getIsread() {
        return $this->isread;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setSubject($subject) {
        $this->subject = $subject;
    }

    function setTo($to) {
        $this->to = $to;
    }

    function setLink($link) {
        $this->link = $link;
    }

    function setIsread($isread) {
        $this->isread = $isread;
    }

    public function getMyVars() {
        return get_object_vars($this);
    }

}
