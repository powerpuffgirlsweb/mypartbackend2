<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Activity
 *
 * @author Pabhoz
 */
class Activity extends BModel {
    

  private $id, $title, $description, $responsable, $team, $expdate, $donedate,
          $status;
  
  
  private $has_one = array(
      'Team'=>array(
          'class'=>'Team',
          'join_as'=>'team',
          'join_with'=>'id'
          ),
      'Responsable'=>array(
          'class'=>'User',
          'join_as'=>'responsable',
          'join_with'=>'id'
          )
      );

      function __construct($id, $title, $description, $responsable, $team, $expdate, $donedate, $status) {
          parent::__construct();
          $this->id = $id;
          $this->title = $title;
          $this->description = $description;
          $this->responsable = $responsable;
          $this->team = $team;
          $this->expdate = $expdate;
          $this->donedate = $donedate;
          $this->status = $status;
      }
      
      function getHas_one() {
          return $this->has_one;
      }

      function setHas_one($has_one) {
          $this->has_one = $has_one;
      }

            
     
    public function getMyVars(){
        return get_object_vars($this);
    }
      
      function getId() {
          return $this->id;
      }

      function getTitle() {
          return $this->title;
      }

      function getDescription() {
          return $this->description;
      }

      function getResponsable() {
          return $this->responsable;
      }

      function getTeam() {
          return $this->team;
      }

      function getExpdate() {
          return $this->expdate;
      }

      function getDonedate() {
          return $this->donedate;
      }

      function getStatus() {
          return $this->status;
      }

  
    

      function setId($id) {
          $this->id = $id;
      }

      function setTitle($title) {
          $this->title = $title;
      }

      function setDescription($description) {
          $this->description = $description;
      }

      function setResponsable($responsable) {
          $this->responsable = $responsable;
      }

      function setTeam($team) {
          $this->team = $team;
      }

      function setExpdate($expdate) {
          $this->expdate = $expdate;
      }

      function setDonedate($donedate) {
          $this->donedate = $donedate;
      }

      function setStatus($status) {
          $this->status = $status;
      }

   


}
