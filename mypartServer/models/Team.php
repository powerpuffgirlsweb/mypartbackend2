<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Team extends BModel {

    private $id, $name, $owner, $picture, $color;

    function __construct($id, $name, $owner, $picture, $color) {
        $this->id = $id;
        $this->name = $name;
        $this->owner = $owner;
        $this->picture = $picture;
        $this->color = $color;
    }

    private $has_one = array(
        'User' => array(
            'class' => 'User',
            'join_as' => 'owner',
            'join_with' => 'id'
        )
    );
    private $has_many = array(
      
        'Users' => array(
            'class' => 'User',
            'my_key' => 'id',
            'other_key' => 'id',
            'join_as' => 'Team',
            'join_with' => 'User',
            'join_table' => 'TeamMembers',
        ),
        'Activities' => array(
            'class' => 'Activity',
            'my_key' => 'id',
            'other_key' => 'id',
            'join_as' => 'Team',
            'join_with' => 'Activity'
        ),
    );

    function getHas_one() {
        return $this->has_one;
    }

    public function getHas_many() {
        return $this->has_many;
    }

    function setHas_one($has_one) {
        $this->has_one = $has_one;
    }

    function setHas_many($has_many) {
        $this->has_many = $has_many;
    }

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getOwner() {
        return $this->owner;
    }

    public function getPicture() {
        return $this->picture;
    }

    public function getColor() {
        return $this->color;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setOwner($owner) {
        $this->owner = $owner;
    }

    public function setPicture($picture) {
        $this->picture = $picture;
    }

    public function setColor($color) {
        $this->color = $color;
    }

    public function getMyVars() {
        return get_object_vars($this);
    }

}
