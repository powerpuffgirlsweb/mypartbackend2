<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Team_controller extends BServiceController {

    
     function __construct() {
        parent::__construct();
    }

     public function getIndex($id = null) {
        if(is_null($id)){
            $r = Team::getAll();    
        }else{ 
            $r =Team::getById($id)->toArray();
        }
        Penelope::printJSON($r);
    }
    
    public function postIndex() {
        
        $act = Team::instanciate($_POST);
        $r = $act->create();
        Penelope::printJSON($r);
    }
    
    public function putStatus(){
        $_PUT = $this->_PUT;
        $id = $_PUT["id"];
        $act = Team::getById($id);
        $act->setStatus(1);
        $r=$act->update();
        Penelope::printJSON($r);
        
    }
    
      public function postMyTeam(){
        
      Request::setHeader(202, "text/json");
      $user = $_POST;
      $response = Team_bl::myTeam($user);
      Penelope::printJSON($response);
        
    }
    
     public function postCreateTeam(){
        
      Request::setHeader(202, "text/json");
      $user = $_POST;
      $response = Team_bl::myTeam($user);
       Penelope::printJSON($response);
        
    }
    
     public function postMyActivities(){
        
      Request::setHeader(202, "text/json");
   

      $user = $_POST;
      $response = Team_bl::MyActivities($user);
       Penelope::printJSON($response);
        
    }
    
     public function getTeam(){
        
      Request::setHeader(202, "text/json");
   
      $response = Team_bl::Teams();
       Penelope::printJSON($response);
        
    }
    
    
    
    
}