<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class User_controller extends BServiceController {

    function __construct() {
        parent::__construct();
    }

    public function getIndex($id = null) {
        if (is_null($id)) {
            $r = User::getAll();
        } else {
            $r = User::getById($id)->toArray();
        }
        Penelope::printJSON($r);
    }

    public function postIndex() {
        $act = User::instanciate($_POST);
        $r = $act->create();
        Penelope::printJSON($r);
    }

    public function putStatus() {
        $_PUT = $this->_PUT;
        $id = $_PUT["id"];
        $act = User::getById($id);
        $act->setStatus(1);
        $r = $act->update();
        Penelope::printJSON($r);
    }

    public function postLogin() {
        Request::setHeader(202, "text/json");
        $user = $_POST;
        $response = User_bl::login($user);
        Penelope::printJSON($response);
    }

    public function postRegistro() {

        Request::setHeader(202, "text/json");
        $teams = $_POST["team"];
        unset($_POST["team"]);
        $user = $_POST;
        $response = User_bl::registro($user);
        
        //Penelope::printJSON($_POST["team"]);
        Penelope::printJSON($response);
    }

    public function postUpdateUser() {

        Request::setHeader(202, "text/json");
        $user = $_POST;

        if (isset($user)) {
            $response = User_bl::update($user);
            Penelope::printJSON($response);
        }
        //acá pones todo lo que haría si existe
        else {
            $response[] = array("token" => "null", "id" => null);
            Penelope::printJSON($response);
        }
    }

    public function postSolicitud() {

        Request::setHeader(202, "text/json");
        $user = $_POST;

            $response = User_bl::solicitud($user);
            Penelope::printJSON($response);
        
    
        
    }

}
