<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Notification_controller extends BServiceController {

    
     function __construct() {
        parent::__construct();
    }
    
     public function getIndex($id = null) {
        if(is_null($id)){
            $r = Notification::getAll();    
        }else{ 
            $r =Notification::getById($id)->toArray();
        }
        Penelope::printJSON($r);
    }
    
    public function postIndex() {
        
        $act = Notification::instanciate($_POST);
        $r = $act->create();
        Penelope::printJSON($r);
    }
    
    public function putStatus(){
        $_PUT = $this->_PUT;
        $id = $_PUT["id"];
        $act = Notification::getById($id);
        $act->setStatus(1);
        $r=$act->update();
        Penelope::printJSON($r);
        
    }
    
}
