import { Co.Usbcali.Edu.MypartPage } from './app.po';

describe('co.usbcali.edu.mypart App', () => {
  let page: Co.Usbcali.Edu.MypartPage;

  beforeEach(() => {
    page = new Co.Usbcali.Edu.MypartPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
