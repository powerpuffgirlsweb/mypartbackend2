import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { Activity } from "models/activity";
import { ActivityService } from "services/activity";

@Component({
  selector: 'app-tareas',
  templateUrl: './tareas.component.html',
  styleUrls: ['./tareas.component.scss']
})
export class TareasComponent implements OnInit {

  constructor(public activityService:ActivityService) { }

  ngOnInit() {
  }

  addActivity(formulario: NgForm){

    let tarea = new Activity(null,formulario.value.title,formulario.value.description,1,1,null,null,0);
    
    this.activityService.post(tarea)
    .subscribe(
      data => { let response = data.json(); if(!response.error){ alert("Actividad Creada!"); }else{alert(response.msg)} },
      err => console.log(err),
      () => console.log("Post Request Lista!")
    );

  }

}
