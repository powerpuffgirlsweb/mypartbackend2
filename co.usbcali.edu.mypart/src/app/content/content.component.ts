import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { AsideComponent } from "app/aside/aside.component";

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {
  
  @Input()
  viewing: any;

  constructor() { }

  ngOnInit() {
  }

  getNotification(data) {
    console.log("getting a notification: "+data);
  }

  getChange(data){
    console.log("Llega esto desde mi hermano aside: "+data);
  }

}
